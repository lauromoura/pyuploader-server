from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseNotFound

import os

from .forms import UploadFileForm
from .models import File
from .serializers import FileSerializer

from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Web Views
def index(request):
    form = UploadFileForm()

    return render(request, 'home.html', {'form': form})


def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = File(file=request.FILES['file'])
            file.save()
        return HttpResponseRedirect('/pyuploader/')
    return HttpResponseNotFound()

# Rest API Views
class FileUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return Response("Not authorized", status=status.HTTP_401_UNAUTHORIZED)

        file_serializer = FileSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
