from django.apps import AppConfig


class PyuploaderConfig(AppConfig):
    name = 'pyuploader'
