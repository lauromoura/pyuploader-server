
from django.contrib import admin
from django.db import models

class File(models.Model):

    file = models.FileField(blank=False, null=False)
    def __str__(self):
        return self.file.name


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    pass
