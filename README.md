PyUploader - Server
===================

Pre-requisites:

* Django 2.2
* Django Rest Framework
* SQlite 3

Preparing the server
--------------------

0. Clone this repository
1. Run the database migrations `python manage.py migrate`
2. Create a superuser to log in with `python manage.py createsuperuser`

Running the server
------------------

1. From the project root, launch `python manage.py runserver 0.0.0.0:8000`
2. Launch the browser to `SERVER_IP:8000/pyuploader`

You may need to add the server ip to `ALLOWED_HOSTS` in `settings.py` in order
to allow access from other machines.
